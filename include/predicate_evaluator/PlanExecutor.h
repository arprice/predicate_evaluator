/**
 * \file PlanExecutor.h
 * \brief
 *
 * \author Andrew Price
 * \date 2015-6-19
 *
 * \copyright
 *
 * Copyright (c) 2015, Georgia Tech Research Corporation
 * All rights reserved.
 *
 * This file is provided under the following "BSD-style" License:
 * Redistribution and use in source and binary forms, with or
 * without modification, are permitted provided that the following
 * conditions are met:
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
 * CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef PLANEXECUTOR_H
#define PLANEXECUTOR_H

#include "PDDLParser.h"
#include <vector>
#include <map>

class PlanExecutor : public VAL::VisitController
{
public:
	PlanExecutor();
	~PlanExecutor();

	bool load(const std::string& domainFiles,
	          const std::string& problemFile,
	          const std::string& solutionFile);


	bool execute();

protected:
	const VAL::plan* mpPlan;
	const VAL::effect_lists* mpInit;
	const VAL::goal* mpGoal;

	std::map<std::string, const VAL::pred_decl*> mPredicateDeclarations;
	std::map<std::string, const VAL::action*> mActionDeclarations;

	virtual void visit_plan_step(const VAL::plan_step *s);

	virtual void visit_pred_decl(const VAL::pred_decl *d);
	virtual void visit_pred_symbol(const VAL::pred_symbol *s);
	virtual void visit_var_symbol(const VAL::var_symbol *s);

	virtual void visit_action(const VAL::action *a);
	virtual void visit_const_symbol(const VAL::const_symbol *s);

	virtual void visit_conj_goal(const VAL::conj_goal *g);
	virtual void visit_simple_goal(const VAL::simple_goal *g);

	virtual void visit_effect_lists(const VAL::effect_lists *l);
	virtual void visit_simple_effect(const VAL::simple_effect *e);
	virtual void visit_cond_effect(const VAL::cond_effect *e);
	virtual void visit_assignment(const VAL::assignment *a);

};

#endif // PLANEXECUTOR_H
