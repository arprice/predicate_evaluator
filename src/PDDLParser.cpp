/**
 * \file PDDLParser.cpp
 * \brief
 *
 * \author Andrew Price
 * \date 2015-6-19
 *
 * \copyright
 *
 * Copyright (c) 2015, Georgia Tech Research Corporation
 * All rights reserved.
 *
 * This file is provided under the following "BSD-style" License:
 * Redistribution and use in source and binary forms, with or
 * without modification, are permitted provided that the following
 * conditions are met:
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
 * CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "predicate_evaluator/PDDLParser.h"

#include <cstdio>
#include <iostream>
#include <fstream>
#include <boost/filesystem.hpp>

#include "FlexLexer.h"

namespace fs = boost::filesystem;

extern int yyparse();
extern int yydebug;

namespace VAL
{
parse_category* top_thing = nullptr;
analysis an_analysis;
analysis* current_analysis;
yyFlexLexer* yfl;
}

const char * current_filename;

PDDLParser* PDDLParser::gParser = nullptr;

PDDLParser::PDDLParser()
{
	VAL::current_analysis = &VAL::an_analysis;
	VAL::yfl= new yyFlexLexer;
}

PDDLParser::~PDDLParser()
{
	delete VAL::yfl;
}

const VAL::parse_category* PDDLParser::parseFile(const std::string& filename, const bool debug)
{
	if (!fs::exists(filename) || !fs::is_regular_file(filename))
	{
		throw std::runtime_error("Unable to locate '" + filename + "'.");
	}

	std::ifstream* ifs = new std::ifstream(filename);
	if (ifs->bad())
	{
		return nullptr;
	}

	current_filename = filename.c_str();
	yydebug= debug ? 1 : 0;

	VAL::yfl->switch_streams(ifs,&std::cout);
	yyparse();

	if (VAL::current_analysis->error_list.size() > 0)
	{
		std::cout << "Error report for '" << filename << "':";
		VAL::current_analysis->error_list.report();
	}

	return VAL::top_thing;
}
