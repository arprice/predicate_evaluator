/************************************************************************
 * Copyright 2008, Strathclyde Planning Group,
 * Department of Computer and Information Sciences,
 * University of Strathclyde, Glasgow, UK
 * http://planning.cis.strath.ac.uk/
 *
 * Maria Fox, Richard Howey and Derek Long - VAL
 * Stephen Cresswell - PDDL Parser
 *
 * This file is part of VAL, the PDDL validator.
 *
 * VAL is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * VAL is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VAL.  If not, see <http://www.gnu.org/licenses/>.
 *
 ************************************************************************/

/*
  main() for the PDDL2.2 parser

  $Date: 2009-02-05 10:50:26 $
  $Revision: 1.2 $

  This expects any number of filenames as arguments, although
  it probably doesn't ever make sense to supply more than two.

  stephen.cresswell@cis.strath.ac.uk

   Strathclyde Planning Group
 */

#include <cstdio>
#include <iostream>
#include <fstream>
#include "ptree.h"
#include "FlexLexer.h"

extern int yyparse();
extern int yydebug;

using std::ifstream;
using std::ofstream;

namespace VAL
{

parse_category* top_thing=NULL;

//analysis an_analysis;
//analysis* current_analysis;

yyFlexLexer* yfl;

}

#include "predicate_evaluator/PlanExecutor.h"

#include "VisitController.h"
#include <vector>

// NB: A predicate is a proposition that depends on the values of its arguments

struct PredicatePrinter : public VAL::VisitController
{
public:

	// Visit domain definitions
	virtual void visit_domain(const VAL::domain *d)
	{
		visitingPlan = false;
		std::cout << "In visit_domain. " << std::endl;
		d->predicates->visit(this);
		d->ops->visit(this);
	}

	virtual void visit_pred_decl(const VAL::pred_decl *d)
	{
		if (!visitingPlan)
		{
			mPredicateDeclarations[d->getPred()->getName()] = d;
		}
		else
		{
			d->getPred()->visit(this);
			d->getArgs()->visit(this);
		}
	}

	virtual void visit_pred_symbol(const VAL::pred_symbol *s)
	{
		std::cout << "\t\tPredicate symbol: '" << s->getName() << "'" << std::endl;
	}

	virtual void visit_var_symbol(const VAL::var_symbol *s)
	{
		std::cout << "\t\t\tVariable symbol: '" << s->getName() << "' of type '" << s->type->getName() << "'" << std::endl;
	}

	virtual void visit_action(const VAL::action *a)
	{
		if (!visitingPlan)
		{
			mActionDeclarations[a->name->getName()] = a;
		}
		else
		{
			std::cout << "\tAction name: '" << a->name->getName() << "'" << std::endl;
			a->precondition->visit(this);
			a->effects->visit(this);
		}
	}

	// Visit problem
	virtual void visit_problem(const VAL::problem *p)
	{
		visitingPlan = false;
		indentation	= "";
		std::cout << "Problem: '" << p->name << "'" << std::endl;
	}

	// Visit plan steps
	virtual void visit_plan_step(const VAL::plan_step *s)
	{
		visitingPlan = true;
		indentation	= "";
		std::cout << "Plan Step: " << s->op_sym->getName() << " (";
		s->params->visit(this);
		std::cout << ")" << std::endl;
		mActionDeclarations[s->op_sym->getName()]->visit(this);
	}

	virtual void visit_const_symbol(const VAL::const_symbol *s)
	{
		std::cout << std::endl << "\t";
		if (s->type)
			std::cout << s->type->getName();
		else
			std::cout << "???";
		std::cout << ": " << s->getName() << " ";
	}

	virtual void visit_conj_goal(const VAL::conj_goal *g)
	{
		g->getGoals()->visit(this);
	}

	virtual void visit_simple_goal(const VAL::simple_goal *g)
	{
		std::cout << "\t\tPrecondition: " << std::endl;
//		g->getProp()->head->visit(this);
		mPredicateDeclarations[g->getProp()->head->getName()]->visit(this);
	}

	virtual void visit_effect_lists(const VAL::effect_lists *l)
	{
		l->add_effects.visit(this);
		l->assign_effects.visit(this);
		l->cond_assign_effects.visit(this);
		l->del_effects.visit(this);

	}

	virtual void visit_simple_effect(const VAL::simple_effect *e)
	{
		std::cout << "\t\tEffect: " << std::endl;
		mPredicateDeclarations[e->prop->head->getName()]->visit(this);
	}

	virtual void visit_cond_effect(const VAL::cond_effect *e)
	{
		std::cout << "\t\tCond Effect: " << std::endl;
		e->getCondition()->visit(this);
	}

	virtual void visit_assignment(const VAL::assignment *a)
	{
		std::cout << "\t\tAssignment: ";
		std::cout << a->getFTerm()->getFunction()->getName() << std::endl;
		//mPredicateDeclarations[e->prop->head->getName()]->visit(this);
	}

protected:
	std::map<std::string, const VAL::pred_decl*> mPredicateDeclarations;
	std::map<std::string, const VAL::action*> mActionDeclarations;

	bool visitingPlan;
	std::string indentation;

};

char * current_filename;

int main(int argc,char * argv[])
{
	VAL::current_analysis= &VAL::an_analysis;
	ifstream* current_in_stream;
	yydebug=0; // Set to 1 to output yacc trace

	VAL::yfl= new yyFlexLexer;
	PredicatePrinter* printer = new PredicatePrinter();
	PlanExecutor executor;

	// Loop over given args
	for (int a=1; a<argc; ++a)
	{
		current_filename= argv[a];
		cout << "File: " << current_filename << '\n';
		current_in_stream= new ifstream(current_filename);
		if (current_in_stream->bad())
		{
			// Output a message now
			cout << "Failed to open\n";

			// Log an error to be reported in summary later
			line_no= 0;
			log_error(VAL::E_FATAL,"Failed to open file");
		}
		else
		{
			line_no= 1;

			// Switch the tokeniser to the current input stream
			VAL::yfl->switch_streams(current_in_stream,&cout);
			yyparse();

			// Output syntax tree
			if (VAL::top_thing)
			{
//				VAL::top_thing->display(0);
				VAL::top_thing->visit(printer);
				//if (executor.loadPlan(VAL::top_thing))
				{
					executor.execute();
				}
			}
		}
		delete current_in_stream;
	}
	// Output the errors from all input files
	VAL::current_analysis->error_list.report();
	delete VAL::yfl;
	delete printer;
}
