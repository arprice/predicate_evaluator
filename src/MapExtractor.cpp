/**
 * \file MapExtractor.cpp
 * \brief
 *
 * \author Andrew Price
 * \date 2015-6-19
 *
 * \copyright
 *
 * Copyright (c) 2015, Georgia Tech Research Corporation
 * All rights reserved.
 *
 * This file is provided under the following "BSD-style" License:
 * Redistribution and use in source and binary forms, with or
 * without modification, are permitted provided that the following
 * conditions are met:
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
 * CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "predicate_evaluator/MapExtractor.h"

MapExtractor::MapExtractor()
{
}

bool MapExtractor::getMaps(const VAL::domain *d,
             std::map<std::string, const VAL::action*>& actionDeclarations,
             std::map<std::string, const VAL::pred_decl*>& predicateDeclarations)
{
	mpActions = &actionDeclarations;
	mpPredicates = &predicateDeclarations;

	d->predicates->visit(this);
	d->ops->visit(this);

	return true;
}

void MapExtractor::visit_pred_decl(const VAL::pred_decl *d)
{
	(*mpPredicates)[d->getPred()->getName()] = d;
}

void MapExtractor::visit_action(const VAL::action *a)
{
	(*mpActions)[a->name->getName()] = a;
}
