/**
 * \file PlanExecutor.cpp
 * \brief
 *
 * \author Andrew Price
 * \date 2015-6-19
 *
 * \copyright
 *
 * Copyright (c) 2015, Georgia Tech Research Corporation
 * All rights reserved.
 *
 * This file is provided under the following "BSD-style" License:
 * Redistribution and use in source and binary forms, with or
 * without modification, are permitted provided that the following
 * conditions are met:
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
 * CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "predicate_evaluator/PlanExecutor.h"
#include "predicate_evaluator/MapExtractor.h"

#include <stdexcept>

PlanExecutor::PlanExecutor()
    : VAL::VisitController(),
      mpPlan(nullptr)
{

}

PlanExecutor::~PlanExecutor()
{
}

bool PlanExecutor::load(const std::string& domainFile,
                        const std::string& problemFile,
                        const std::string& solutionFile)
{
	const VAL::parse_category* result = nullptr;
	const VAL::domain* domain = nullptr;
	const VAL::problem* problem = nullptr;

	result = PDDLParser::Instance()->parseFile(domainFile);
	if ((domain = dynamic_cast<const VAL::domain*>(result)))
	{
		MapExtractor me;
		me.getMaps(domain, mActionDeclarations, mPredicateDeclarations);
	}
	else
	{
		throw std::runtime_error("Failed to parse domain file '" + domainFile + "'.");
	}

	result = PDDLParser::Instance()->parseFile(problemFile);
	if ((problem = dynamic_cast<const VAL::problem*>(result)))
	{
		mpInit = problem->initial_state;
		mpGoal = problem->the_goal;
	}
	else
	{
		throw std::runtime_error("Failed to parse problem file '" + problemFile + "'.");
	}

	result = PDDLParser::Instance()->parseFile(solutionFile);
	if (!(mpPlan = dynamic_cast<const VAL::plan*>(result)))
	{
		throw std::runtime_error("Failed to parse solution file '" + solutionFile + "'.");
	}

	return true;
}

bool PlanExecutor::execute()
{
	if (nullptr == mpPlan)
	{
		throw std::runtime_error("No valid plan loaded.");
	}

	std::cout << "Initial State:" << std::endl;
	mpInit->visit(this);

	std::cout << "Plan Steps: " << mpPlan->size() << std::endl;
	mpPlan->visit(this);

	std::cout << "Goal State:" << std::endl;
	mpGoal->visit(this);

	return true;
}

void PlanExecutor::visit_plan_step(const VAL::plan_step *s)
{
	std::cout << "Plan Step: " << s->op_sym->getName() << " (";
	s->params->visit(this);
	std::cout << ")" << std::endl;
	mActionDeclarations[s->op_sym->getName()]->visit(this);
}

void PlanExecutor::visit_pred_decl(const VAL::pred_decl *d)
{
	d->getPred()->visit(this);
	d->getArgs()->visit(this);
}

void PlanExecutor::visit_pred_symbol(const VAL::pred_symbol *s)
{
	std::cout << "\t\tPredicate symbol: '" << s->getName() << "'" << std::endl;
}

void PlanExecutor::visit_var_symbol(const VAL::var_symbol *s)
{
	std::cout << "\t\t\tVariable symbol: '" << s->getName() << "' of type '" << s->type->getName() << "'" << std::endl;
}

void PlanExecutor::visit_action(const VAL::action *a)
{
	std::cout << "\tAction name: '" << a->name->getName() << "'" << std::endl;
	a->precondition->visit(this);
	a->effects->visit(this);
}

void PlanExecutor::visit_const_symbol(const VAL::const_symbol *s)
{
	std::cout << std::endl << "\t";
	if (s->type)
		std::cout << s->type->getName();
	else
		std::cout << "???";
	std::cout << ": " << s->getName() << " ";
}

void PlanExecutor::visit_conj_goal(const VAL::conj_goal *g)
{
	g->getGoals()->visit(this);
}

void PlanExecutor::visit_simple_goal(const VAL::simple_goal *g)
{
	std::cout << "\t\tPrecondition: " << std::endl;
	mPredicateDeclarations[g->getProp()->head->getName()]->visit(this);
}

void PlanExecutor::visit_effect_lists(const VAL::effect_lists *l)
{
	l->add_effects.visit(this);
	l->assign_effects.visit(this);
	l->cond_assign_effects.visit(this);
	l->cond_effects.visit(this);
	l->del_effects.visit(this);
	l->forall_effects.visit(this);
}

void PlanExecutor::visit_simple_effect(const VAL::simple_effect *e)
{
	std::cout << "\t\tEffect: " << std::endl;
	mPredicateDeclarations[e->prop->head->getName()]->visit(this);
}

void PlanExecutor::visit_cond_effect(const VAL::cond_effect *e)
{
	std::cout << "\t\tCond Effect: " << std::endl;
	e->getCondition()->visit(this);
}

void PlanExecutor::visit_assignment(const VAL::assignment *a)
{
	std::cout << "\t\tAssignment: ";
	std::cout << a->getFTerm()->getFunction()->getName() << std::endl;
	//mPredicateDeclarations[e->prop->head->getName()]->visit(this);
}


